/* Get two elements from DOM */
const pokeballImage = document.getElementById('pokeballClick');
const charizardImage = document.getElementById('charizardClick');

/* After click in charizard, appear Pokeball but then 1.5 seconds charizard escape from pokeball 
    appearImagen and RemoveImage allow show and hide the images
*/ 
charizardImage.addEventListener('click', () => {
    if( charizardImage.classList.contains('appearImage') ){
        charizardImage.classList.remove('appearImage');
        pokeballImage.classList.add('appearImage');
        pokeballImage.classList.remove('dissapearImage');
        charizardImage.classList.add('dissapearImage');
        setTimeout(() => {
            pokeballImage.classList.add('dissapearImage');
            pokeballImage.classList.remove('appearImage');
            charizardImage.classList.remove('dissapearImage');
            charizardImage.classList.add('appearImage');
        }, 1500);
    }
})
